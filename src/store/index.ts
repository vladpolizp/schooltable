import { configureStore } from "@reduxjs/toolkit";
import attendanceReducer from "./modules/attendance/reducer";

const store = configureStore({ reducer: { attendance: attendanceReducer } });

export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
export default store;
