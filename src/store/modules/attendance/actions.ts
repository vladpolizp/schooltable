import {
  getAttendance,
  getLessons,
  getStudents,
  setAttendance,
} from "api/attendanceApi";
import { SET_ATTENDANCE, SET_LESSONS, SET_STUDENTS } from "./Types";
import { AppDispatch } from "../../index";

export const getStudentsAction = () => async (dispatch: AppDispatch) => {
  const res = await getStudents();
  return dispatch({
    type: SET_STUDENTS,
    payload: { items: res.data.Items, quantity: res.data.Quantity },
  });
};
export const getLessonsAction = () => async (dispatch: AppDispatch) => {
  const res = await getLessons();
  return dispatch({
    type: SET_LESSONS,
    payload: res.data.Items,
  });
};
export const getAttendanceAction = () => async (dispatch: AppDispatch) => {
  const res = await getAttendance();
  return dispatch({
    type: SET_ATTENDANCE,
    payload: res.data.Items,
  });
};
export const setStudentAttendanceAction =
  (schoolBoyId: number, lessonId: number, isAttendance: boolean) =>
  async (dispatch: AppDispatch) => {
    await setAttendance(schoolBoyId, lessonId, isAttendance);
    dispatch(getAttendanceAction());
  };
