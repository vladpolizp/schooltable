export interface Student {
  Id: number;
  FirstName: string;
  SecondName: string;
  LastName: string;
}

export interface Lesson {
  Id: number;
  Title: string;
}
export interface Attendance {
  Id: number;
  Title: string;
  SchoolboyId: number;
  ColumnId: number;
}

export interface AttendanceState {
  students: { items: Student[]; quantity: number };
  lessons: Lesson[];
  attendance: Attendance[];
}
