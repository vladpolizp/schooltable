import { AttendanceState } from "./interfaces";
import { SET_ATTENDANCE, SET_LESSONS, SET_STUDENTS } from "./Types";

const initialState: AttendanceState = {
  lessons: [],
  students: {
    items: [],
    quantity: 0,
  },
  attendance: [],
};

const attendanceReducer = (
  state: AttendanceState = initialState,
  action: { type: string; payload?: any },
) => {
  switch (action.type) {
    case SET_STUDENTS:
      return {
        ...state,
        students: {
          items: action.payload.items,
          quantity: action.payload.quantity,
        },
      };
    case SET_LESSONS:
      return {
        ...state,
        lessons: action.payload,
      };
    case SET_ATTENDANCE:
      return {
        ...state,
        attendance: action.payload,
      };
    default:
      return state;
  }
};

export default attendanceReducer;
