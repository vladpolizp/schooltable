import { RootState } from "store";
export const studentsSelector = (state: RootState) => ({
  items: state.attendance.students.items,
  quantity: state.attendance.students.quantity,
});

export const lessonsSelector = (state: RootState) => state.attendance.lessons;
export const attendanceSelector = (state: RootState) =>
  state.attendance.attendance;
