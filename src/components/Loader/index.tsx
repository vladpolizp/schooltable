import React from "react";
import CircularProgress from "@mui/material/CircularProgress";
import classes from "./Loader.module.scss";
const Loader = () => {
  return (
    <div className={classes.container}>
      <CircularProgress />
    </div>
  );
};

export default Loader;
