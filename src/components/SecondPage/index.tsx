import React, { useEffect, useMemo, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import {
  lessonsSelector,
  studentsSelector,
} from "../../store/modules/attendance/selectors";
import { shallowEqual } from "react-redux";
import {
  getLessonsAction,
  getStudentsAction,
} from "../../store/modules/attendance/actions";
import { Lesson } from "../../store/modules/attendance/interfaces";
import Loader from "../Loader";

const SecondPage: () => JSX.Element = () => {
  const students = useAppSelector(studentsSelector, shallowEqual);
  const [isLoading, setIsLoading] = useState(true);

  const lessons = useAppSelector(lessonsSelector, shallowEqual);

  const dispatch = useAppDispatch();

  const theBiggestColumn = useMemo((): Lesson => {
    return lessons.reduce((accumulator: Lesson, currentValue: Lesson) => {
      const firstNumbers = accumulator.Title.split("/");
      const nextNumbers = currentValue.Title.split("/");

      return firstNumbers[0] + firstNumbers[1] < nextNumbers[0] + nextNumbers[1]
        ? currentValue
        : accumulator;
    }, lessons[0]);
  }, [lessons]);

  useEffect(() => {
    Promise.all([
      !students.quantity ? dispatch(getStudentsAction()) : undefined,
      !lessons.length ? dispatch(getLessonsAction()) : undefined,
    ]).then(() => {
      setIsLoading(false);
    }); //eslint-disable-next-line
  }, []);

  return isLoading ? (
    <Loader />
  ) : (
    <>
      <h1>Hello it`s my test project</h1>
      <p>Students: {students.quantity}</p>
      {JSON.stringify(theBiggestColumn)}
    </>
  );
};

export default SecondPage;
