import React, {
  MouseEventHandler,
  useCallback,
  useEffect,
  useState,
} from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@mui/material";
import classes from "components/SchoolTable/SchoolTable.module.scss";
import { shallowEqual } from "react-redux";
import {
  attendanceSelector,
  lessonsSelector,
  studentsSelector,
} from "store/modules/attendance/selectors";
import {
  getAttendanceAction,
  getLessonsAction,
  getStudentsAction,
  setStudentAttendanceAction,
} from "store/modules/attendance/actions";
import {
  Attendance,
  Lesson,
  Student,
} from "store/modules/attendance/interfaces";
import clsx from "clsx";
import { useAppDispatch, useAppSelector } from "store/hooks";
import Loader from "components/Loader";

const SchoolTable = (): JSX.Element => {
  const dispatch = useAppDispatch();

  const [isLoading, setIsLoading] = useState(true);

  const students = useAppSelector(studentsSelector, shallowEqual);

  const lessons = useAppSelector(lessonsSelector, shallowEqual);

  const attendance = useAppSelector(attendanceSelector, shallowEqual);

  const onVisitClick: (
    schoolBoyId: number,
    lessonId: number,
    isAttendance: boolean,
  ) => MouseEventHandler = useCallback(
    (schoolBoyId, lessonId, isAttendance) => async () => {
      dispatch(setStudentAttendanceAction(schoolBoyId, lessonId, isAttendance));
    },
    [dispatch],
  );

  useEffect(() => {
    Promise.all([
      dispatch(getStudentsAction()),
      dispatch(getLessonsAction()),
      dispatch(getAttendanceAction()),
    ]).then(() => {
      setIsLoading(false);
    }); //eslint-disable-next-line
  }, []);

  return isLoading ? (
    <Loader />
  ) : (
    <>
      <div className={classes.container}>
        <Table className={classes.table} stickyHeader>
          <TableHead className={classes.tableHead}>
            <TableRow className={classes.tableRow}>
              <TableCell
                align="center"
                className={clsx(classes.tableCell, classes.numberCell)}
              >
                №
              </TableCell>
              <TableCell
                className={clsx(classes.tableCell, classes.childrenCell)}
              >
                Name
              </TableCell>
              {lessons.map((column: Lesson) => (
                <TableCell
                  align="center"
                  className={clsx(classes.tableCell, classes.columnCell)}
                  key={column.Id}
                >
                  {column.Title}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {students.items.map((student: Student, rowIndex: number) => (
              <TableRow key={student.Id} className={classes.tableRow}>
                <TableCell
                  align="center"
                  className={clsx(classes.tableCell, classes.numberCell)}
                >
                  {rowIndex + 1}
                </TableCell>
                <TableCell
                  className={clsx(classes.tableCell, classes.childrenCell)}
                >{`${student.FirstName || ""} ${student.SecondName || ""} ${
                  student.LastName || ""
                }`}</TableCell>
                {lessons.map((column: Lesson, columnIndex: number) => {
                  const isAttendance = !!attendance.find(
                    (attendance: Attendance) =>
                      attendance.SchoolboyId === student.Id &&
                      attendance.ColumnId === column.Id,
                  );
                  return (
                    <TableCell
                      align="center"
                      className={clsx(classes.tableCell, classes.columnCell)}
                      key={`${rowIndex}-${columnIndex}`}
                      onClick={onVisitClick(
                        student.Id,
                        column.Id,
                        !isAttendance,
                      )}
                    >
                      {isAttendance ? "H" : ""}
                    </TableCell>
                  );
                })}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    </>
  );
};

export default SchoolTable;
