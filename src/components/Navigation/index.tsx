import React from "react";
import { NavLink } from "react-router-dom";
import clsx from "clsx";
import classes from "./Navigation.module.scss";
const Navigation = () => {
  return (
    <div className={classes.linkContainer}>
      <NavLink
        className={({ isActive }) =>
          clsx(classes.linkBtn, { [classes.activeLink]: isActive })
        }
        to="/"
      >
        Go to the table
      </NavLink>
      <NavLink
        className={({ isActive }) =>
          clsx(classes.linkBtn, { [classes.activeLink]: isActive })
        }
        to="/result"
      >
        Go to the second page
      </NavLink>
    </div>
  );
};

export default Navigation;
