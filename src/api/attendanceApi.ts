import axiosInstance from "./index";
import { AxiosResponse } from "axios";
import {
  Attendance,
  Lesson,
  Student,
} from "store/modules/attendance/interfaces";
export const getStudents: () => Promise<
  AxiosResponse<{ Items: Student[]; Quantity: number }>
> = () => axiosInstance.get("Schoolboy");

export const getLessons: () => Promise<
  AxiosResponse<{ Items: Lesson[]; Quantity: number }>
> = () => axiosInstance.get("Column");
export const getAttendance: () => Promise<
  AxiosResponse<{ Items: Attendance[]; Quantity: number }>
> = () => axiosInstance.get("Rate");
export const setAttendance: (
  schoolBoyId: number,
  lessonId: number,
  isAttendance: boolean,
) => Promise<AxiosResponse> = (schoolBoyId, lessonId, isAttendance) =>
  axiosInstance.post(isAttendance ? "Rate" : "UnRate", {
    SchoolboyId: schoolBoyId,
    ColumnId: lessonId,
    Title: isAttendance ? "H" : undefined,
  });
