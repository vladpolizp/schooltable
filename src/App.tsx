import React, { Suspense } from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";
import Loader from "components/Loader";
import Navigation from "./components/Navigation";
const SchoolTable = React.lazy(() => import("components/SchoolTable"));
const SecondPage = React.lazy(() => import("components/SecondPage"));
function App() {
  return (
    <Router>
      <Suspense fallback={<Loader />}>
        <Navigation />
        <Routes>
          <Route path="/" element={<SchoolTable />} />
          <Route path="/result" element={<SecondPage />} />
        </Routes>
      </Suspense>
    </Router>
  );
}

export default App;
